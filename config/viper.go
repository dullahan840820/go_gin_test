package config

import (
	"fmt"

	"github.com/spf13/viper"
)

// Viper 讀取設定檔
func Viper() {
	viper.SetConfigName("app")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./config")
	viper.SetDefault("application.port", 8080)
	err := viper.ReadInConfig()
	if err != nil {
		panic("讀取設定檔出現錯誤，原因為：" + err.Error())
	}
	fmt.Println("application port = " + viper.GetString("application.port"))
}
