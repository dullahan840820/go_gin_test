package db

import (
	"database/sql"
	"fmt"
	"log"
	"src/go_gin_test/model"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

const (
	USERNAME = "root"
	PASSWORD = "rootroot"
	NETWORK  = "tcp"
	SERVER   = "127.0.0.1"
	PORT     = 3306
	DATABASE = "demo"
)

type User struct {
	ID       int64  `json:"id" gorm:"primary_key:auto_increase"`
	Username string `json:"username"`
	Password string `json:"password"`
	Email    string `json:"email"`
	Phone    string `json:"phone"`
}

// mysql driver 內建建立table
func CreateTable(db *sql.DB) error {
	sql := `CREATE TABLE IF NOT EXISTS users(
	id INT(4) PRIMARY KEY AUTO_INCREMENT NOT NULL,
        username VARCHAR(64),
        password VARCHAR(64)
	); `

	if _, err := db.Exec(sql); err != nil {
		fmt.Println("建立 Table 發生錯誤:", err)
		return err
	}
	fmt.Println("建立 Table 成功！")
	return nil
}

// Exec 建立會員
func CreateUser(db *gorm.DB, user *model.User) error {
	return db.Create(user).Error
}

// 搜尋會員
func FindUser(db *gorm.DB, id int64) (*model.User, error) {
	user := new(model.User)
	user.ID = id
	err := db.First(&user).Error
	return user, err
}

// Gorm 應用
func DB() {
	//SQL
	dsn := fmt.Sprintf("%s:%s@%s(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", USERNAME, PASSWORD, NETWORK, SERVER, PORT, DATABASE)

	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("使用 gorm 連線 DB 發生錯誤，原因為 " + err.Error())
	}

	if err := db.AutoMigrate(new(User)); err != nil {
		panic("資料庫 Migrate 失敗，原因為 " + err.Error())
	}

	if err := CreateUser(db, &model.User{
		Username: "test",
		Password: "test",
	}); err != nil {
		panic("資料庫 Migrate 失敗，原因為 " + err.Error())
	}

	if user, err := FindUser(db, 1); err == nil {
		log.Println("查詢 User 為", user)
	} else {
		panic("查詢 user 失敗，原因為 " + err.Error())
	}

}
