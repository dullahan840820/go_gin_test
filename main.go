package main

import (
	"errors"
	"fmt"
	netHttp "net/http"
	"src/go_gin_test/login"
	"src/go_gin_test/model"
	"time"

	"src/go_gin_test/module/user/delivery/http"
	"src/go_gin_test/module/user/repository"
	"src/go_gin_test/module/user/service"

	"github.com/codingXiang/configer"
	cx "github.com/codingXiang/cxgateway/delivery/http"
	"github.com/codingXiang/go-logger"
	"github.com/codingXiang/go-orm"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type IndexData struct {
	Title   string
	Content string
}

type LoginInfo struct {
	UserID     int64     `json:"userId"`
	ClientIP   string    `json:"clientIP"`
	LoginState string    `json:"loginState"`
	LoginTime  time.Time `json:"loginTime"`
}

func NewLoginInfo(id int64, clientIP string, loginState string) *LoginInfo {
	return &LoginInfo{
		UserID:     id,
		ClientIP:   clientIP,
		LoginState: loginState,
		LoginTime:  time.Now(),
	}
}

func init() {
	var err error
	config := configer.NewConfigerCore("yaml", "config", "./config", ".")

	config.SetAutomaticEnv("")
	// 初始化Gateway
	cx.Gateway = cx.NewApiGateway("config", config)

	// 初始化db參數
	db := configer.NewConfigerCore("yaml", "database", "./config", ".")
	db.SetAutomaticEnv("")
	configer.Config.AddCore("db", db)
	// 設定資料庫
	if orm.DatabaseORM, err = orm.NewOrm("database", configer.Config.GetCore("db")); err == nil {
		// 	建立 Table Schema (Module)
		logger.Log.Debug("setup table schema")
		{
			// 設定使用者
			orm.DatabaseORM.CheckTable(true, &model.User{})
		}
	} else {
		logger.Log.Error(err.Error())
		panic(err.Error())
	}

}
func test(c *gin.Context) {
	data := new(IndexData)
	data.Title = "首頁"
	data.Content = "我的第一個首頁"
	c.HTML(netHttp.StatusOK, "index.html", data)
}

func LoginPage(c *gin.Context) {
	c.HTML(netHttp.StatusOK, "login.html", nil)
}

func LoginAuth(c *gin.Context) {
	var (
		username string
		password string
	)

	if in, isExist := c.GetPostForm("username"); isExist && in != "" {
		username = in
	} else {
		c.HTML(netHttp.StatusBadRequest, "login.html", gin.H{
			"error": errors.New("必須輸入使用者名稱"),
		})
		return
	}
	if in, isExist := c.GetPostForm("password"); isExist && in != "" {
		password = in
	} else {
		c.HTML(netHttp.StatusBadRequest, "login.html", gin.H{
			"error": errors.New("必須輸入密碼名稱"),
		})
		return
	}

	if err := login.Auth(username, password); err == nil {
		c.HTML(netHttp.StatusOK, "login.html", gin.H{
			"success": "登入成功",
		})
		return
	} else {
		c.HTML(netHttp.StatusUnauthorized, "login.html", gin.H{
			"error": err,
		})
		return
	}

}

func main() {
	session, err := mgo.Dial("127.0.0.1:27017")
	if err != nil {
		panic(err)
	}
	db1 := session.DB("user")
	c := db1.C("login.info")

	err = c.Insert(NewLoginInfo(1234, "127.0.0.1", "success"))
	if err != nil {
		panic(err)
	}

	var loginHistory []LoginInfo
	err = c.Find(bson.M{"userid": 1234}).All(&loginHistory)
	for _, history := range loginHistory {
		fmt.Println(history)
	}

	fmt.Println("==========================")

	var lastLogin LoginInfo
	err = c.Find(bson.M{"userid": 1234}).Sort("-logintime").One(&lastLogin)
	if err != nil {
		panic(err)
	}
	fmt.Println(lastLogin)

	// 建立 Repository
	logger.Log.Debug("Create Repository Instance")
	var (
		db       = orm.DatabaseORM.GetInstance()
		userRepo = repository.NewUserRepository(db)
	)
	// 建立 Service
	logger.Log.Debug("Create Service Instance")
	var (
		userSvc = service.NewUserService(userRepo)
	)
	// 建立 Handler (Module)
	logger.Log.Debug("Create Http Handler")
	{
		http.NewUserHttpHandler(cx.Gateway, userSvc)
	}
	cx.Gateway.Run()

}
