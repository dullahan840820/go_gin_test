package i18n

import (
	"fmt"

	. "github.com/codingXiang/gogo-i18n"
	"golang.org/x/text/language"
)

func I18n() {
	GGi18n = NewGoGoi18n(language.English)
	GGi18n.SetFileType("yaml")
	GGi18n.LoadTranslationFile("./i18n",
		language.English)

	msg := GGi18n.GetMessage("welcome", map[string]interface{}{
		"username": "Hong",
	})
	fmt.Println(msg)

	GGi18n.SetUseLanguage(language.English)
	msg = GGi18n.GetMessage("welcome", map[string]interface{}{
		"username": "Hong",
	})
	fmt.Println(msg)
}
